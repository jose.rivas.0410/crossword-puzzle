# AppddictionCodingChallengeNew

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.2.6.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.


## Steps to build
1. Run Angular cli comand "ng new 'application-nname'"
2. Install Bootstrap with command "ng add @ng-bootstrap/ng-bootstrap"
3. Install Angular material by using " ng add @angular/material"
4. Build out the components for the welcome page, puzzle page, and login page
5. Connect to Spring Boot server in the src/environments/environment.ts file
5a. In the environment.ts file, add the code line "apiUrl: 'http://localhost:8080/'" and any other imports from the service file for api callings from the back-end.
6. Run the application with "ng s" and see if everything is working
7. Troubleshoot any errors if they may arrise
8. If everything is working properly, work on the stories and get them knocked out to progress further with the application.
