import { Component, OnInit } from '@angular/core';
import { interval, Observable, Subscription } from 'rxjs';
import { Tile } from '../tile';
import { TileService } from '../tile.service';

@Component({
  selector: 'app-crossword-page',
  templateUrl: './crossword-page.component.html',
  styleUrls: ['./crossword-page.component.css']
})
export class CrosswordPageComponent implements OnInit {
  private timer: Observable<number>;
  time!: number;
  ssoEntered!: boolean;

  private _clockSubscription!: Subscription;

    // words[i] correlates to clues[i]
    private words = ["Direction", "HermannLemp", "elijanney",
      "network", "imap", "boolean", "blockchain", "spoofing", "encryption", "peertopeer",
      "flowchart", "boundary", "Evolution", "Protocol", "optoelectronics",
      "Superconductivity", "photovoltaic", "EdmondBecquerel", "powertower", "england"
    ];
    private clues = ["Vector quantities have a _______",
      "He is credited as the inventor of the modern system of diesel electric traction co-ordination and control",
      "The inventor of the modern knuckle coupler",
      "A collection of computers connected together",
      "Protocol for receiving email",
      "A datatype used to hold one of two types",
      "Distributed ledger, decentralized are terms used in respect to which technology",
      "A website that successfully masquerades as another by falsifying data and is thereby treated as a trusted system by a user or another program",
      "Public and symmetric are types of _____",
      "A network where there is no dedicated server, all nodes are equal",
      "Graphical representation of an algorithm",
      "Test data that is on the edge between valid and invalid data.",
      "Long term _______ is a standard for wireless broadband communication",
      "A set of rules for how computers should communicate",
      "Name of technology whereby a glass fiber carries as much information as hundreds of copper wires",
      "Loss of electrical resistance can lead to _______",
      "_______ is greek for light-electricity",
      "The first person to realize that sunlight could produce an electric current in a solid material in 1839",
      "A type of concentrating solar thermal power systems",
      "The first full scale locomotive engine was setup in this country"
    ];

  tiles!: Tile[];

  constructor(private tileService: TileService) {
    this.timer = interval(1000);
  }

  ngOnInit(): void {
    this.getTiles();
    this._clockSubscription = this.timer.subscribe(time => this.time = time);
    this.ssoEntered = true;
  }

  ngOnDestroy(): void {
    this._clockSubscription.unsubscribe();
  }

  onSubmit(ssoValue: any) {
    let correctResponseCount = 0;
    if(!ssoValue) {
      this.ssoEntered = false;
      console.log('SSO not entered');
    } else {
      correctResponseCount = this.tileService.calculateScore();
      console.log('Length : ' + correctResponseCount);
      console.log(ssoValue);
    }
  }

  getTiles() {
    this.tiles = this.tileService.getTiles();
  }

  updateBox(response: any, tile: { rows: any; cols: any; editable: any; color: any; value: any; }) {
    console.log(response);
    console.log(tile);
    let responseTile: Tile = {
      rows: tile.rows,
      cols: tile.cols,
      editable: tile.editable,
      color: tile.color,
      value: tile.value,
      response: response
    }
    this.tileService.updateResponses(responseTile);
  }

}
