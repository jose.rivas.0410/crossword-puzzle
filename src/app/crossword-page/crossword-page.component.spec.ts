import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CrosswordPageComponent } from './crossword-page.component';

describe('CrosswordPageComponent', () => {
  let component: CrosswordPageComponent;
  let fixture: ComponentFixture<CrosswordPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CrosswordPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CrosswordPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
