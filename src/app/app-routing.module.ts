import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CrosswordPageComponent } from './crossword-page/crossword-page.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { WelcomePageComponent } from './welcome-page/welcome-page.component';

const routes: Routes = [
  {
    path: '',
    component: WelcomePageComponent,
    pathMatch: "full"
  },
  {
    path: 'login',
    component: LoginPageComponent,
    pathMatch: "full"
  },
  {
    path: 'crossword-puzzle',
    component: CrosswordPageComponent,
    pathMatch: "full"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
